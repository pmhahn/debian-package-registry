Debian supports multiple compression algorithms;
- none (>= 1.10.24)
- gzip
- bzip2 (>= 1.10.24, << 1.18.11)
- lzma (>= 1.13.25, << 1.18.11)
- xz (>= 1.15.6)
- zstd (>= 1.21.18)

Different tools like <man:dpkg-source(1)> and <man:dpkg-deb(1)> have varying support:
- all algorithms are supported for extracting
- `dpkg-source -b` can build *source* packages with all algorithms
- `dpkg-deb` deprecated `bzip2` and `lzma` for building *binary* packages

The [Gitlab Debian package registry](https://docs.gitlab.com/ee/user/packages/debian_repository/) does not support all formats:
- it will show up as a `debian-temporary-package`
- it will show the :warning: Error publishing
- the mouseover shows `Invalid Package: failed metadata extraction`

# Requirement
- The [Debian API](https://docs.gitlab.com/ee/user/packages/debian_repository/#enable-the-debian-api) must be enabled for this project by an Administrator until [#337288](https://gitlab.com/gitlab-org/gitlab/-/issues/337288) is resolved.
